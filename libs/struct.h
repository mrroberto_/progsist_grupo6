#ifndef _STRUCT_H_
#define _STRUCT_H_

#include <stdio.h>
#include <stdlib.h>

//******************************* Task  *******************************

struct Task {
  int left_seconds;
  int total_seconds;
  char type; // P --> Print, E --> Email, S --> Input/Output
  int state; // 1 --> InProcess, 2 --> Processed
};

Task newTask(int, char);
void addTask(Task[], char);
void checkTasks(Task[], Task[]);
void doneTask(Task, Task[]);
void showCPU(int, Task[], Task[], Task[], Task[]);

#endif