#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <windows.h>
#include <io.h>
#include "struct.h"

int main (int argc, char **argv) {
  int left_seconds = 0;
  int run_task = 3; // seconds to run new task
  Task print[100], email[100], i_o[100];
  Task proccess_done[300];

  // Set time
  srand(time(NULL));


  while (true) {

    if (left_seconds == 60) return 0;


    if (run_task == 3){
      // Run a new task in every proccess
      addTask(print, 'P');
      addTask(email, 'E');
      addTask(i_o, 'S');
    }


    // Clean screen
    system("clear");
    showCPU(left_seconds, print, email, i_o, proccess_done);


    // sleep 1 second
    Sleep(1);

    if ( run_task == 1 ) run_task = 3;
    else run_task--;

    left_seconds++;

    checkTasks(print, proccess_done); // Print
    checkTasks(email, proccess_done); // Email
    checkTasks(i_o, proccess_done); // I/O

  }

  return 0;
}
