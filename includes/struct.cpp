#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "struct.h"


//******************************* Task  *******************************

Task newTask(int seconds, char type) {
  Task task;
  task.left_seconds = seconds;
  task.total_seconds = seconds;
  task.type = type;
  task.state = 1;

  return task;
}

void addTask(Task task[], char type) {
  for(int i=0; i<100; i++){
    Task t = task[i];

    if (t.type == '\0'){
      int seconds = (rand() % 7) + 1;
      task[i] = newTask(seconds, type);

      return;
    }
  }
}

void doneTask(Task task, Task proccess_done[]){
  for(int i=0; i<300; i++){
    Task t = proccess_done[i];

    if (t.type == '\0'){
      proccess_done[i] = task;
      return;
    }
  }
}

void checkTasks(Task task[], Task proccess_done[]){
  for (int i=0; i<100; i++){
    Task t = task[i];

    if (t.type == '\0') return;

    if (t.state == 1) {
      task[i].left_seconds--;

      if (task[i].left_seconds == 0){
        task[i].state = 2;
        doneTask(task[i], proccess_done);
      }
      return;
    }
  }
}


void showTasksState(Task print[], Task email[], Task i_o[]){
  char print_state[50] = "", email_state[50] = "", i_o_state[50] = "";
  char buff[5];

  for (int i=0; i<100; i++){
    Task p = print[i];
    Task e = email[i];
    Task s = i_o[i];

    // Print proccess
    if (p.state==1) {
      sprintf(buff, "%d", p.left_seconds);

      if (strlen(print_state) == 0) {
        strcpy(print_state, buff);
      } else {
        strcpy(print_state, print_state);

        strcat(print_state, " -> ");
        strcat(print_state, buff);
      }

    }

    // Email proccess
    if (e.state==1) {
      sprintf(buff, "%d", e.left_seconds);

      if (strlen(email_state) == 0) {
        strcpy(email_state, buff);
      } else {
        strcat(email_state, " -> ");
        strcat(email_state, buff);
      }
    }

    // I/O proccess
    if (s.state==1) {
      sprintf(buff, "%d", s.left_seconds);

      if (strlen(i_o_state) == 0) {
        strcpy(i_o_state, buff);
      } else {
        strcat(i_o_state, " -> ");
        strcat(i_o_state, buff);
      }
    }

  }

  // Problem to convert zero with sprintf
  if (strlen(print_state)==0) strcpy(print_state, "0");
  if (strlen(email_state)==0) strcpy(email_state, "0");
  if (strlen(i_o_state)==0) strcpy(i_o_state, "0");

  printf("Impresion: %s\n", print_state);
  printf("Email:     %s\n", email_state);
  printf("E/S:       %s\n\n", i_o_state);

}


void showTasksDone(Task proccess_done[]){
  int print = 0, email = 0, i_o = 0;
  for (int i=0; i<300; i++){
    Task t = proccess_done[i];
    if (t.type == '\0') break;

    if (t.type == 'P') print++;
    else if (t.type == 'E') email++;
    else if (t.type == 'S') i_o++;
  }

  printf("Tareas atendidas de Impresion: %i\n", print);
  printf("Tareas atendidas de Email:     %i\n", email);
  printf("Tareas atendidas de E/S:       %i\n", i_o);
}


void showCPU(int left_seconds, Task print[],
    Task email[], Task i_o[], Task proccess_done[]){

  printf("********* SIMULACION CPU *********\n");
  printf("Tiempo maximo de ejecucion: 60s\n");
  printf("Actualmente: %is\n\n", left_seconds);

  // Show all procces with state 'InProccess'
  showTasksState(print, email, i_o);

  // Show all task done
  showTasksDone(proccess_done);
}